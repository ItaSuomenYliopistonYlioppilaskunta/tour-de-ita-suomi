# Tour de itä-suomi kuopio

Muutoshistoria: [tästä](https://gitlab.com/ItaSuomenYliopistonYlioppilaskunta/tour-de-ita-suomi/-/commits/master)

	lauantaina 18.9.
	klo 08:00 - 17:00 voi päättyä aikaisemmin 

	Lähtö lukemalta tai lukemalta opastetussa paikassa mikäli tietöitä esteenä
	maali lukemalla
	ei ajanottoa
	tapahtumaluvat ja ilmoitukset: niklas

	huoltopiste: { 
		nimettömällä parkkipaikalla päivärannantien varressa: 62.95224, 27.68169
		tarjolla vettä ja mahdollisesti jotain sponsoroitua?
		tarvitaan pöytä
	}


	reitit: {
		tarkka reititys saattaa poiketa tähän kirjatusta mikäli merkitessä havaitaan esimerkiksi pyöräteitä tms.
		reittejä ei ole vielä julkaistu osallistujille, vain matkat on jonka perusteella pystyy valmistautumaan
		
		kartat: https://www.google.com/maps/d/u/1/edit?mid=1X7wVy7MBWGlsU7w-d8wyPglJsy_NCQTu&usp=sharing
		
		20km {
			Lukema
			Puijonlaaksontie
			Kallantie
			Päivärannantie
			Huoltopiste
			Selluntie
			Virranniementie
			Laivaväyläntie
			Päivärannantie
			... saapumisreittiä takaisin
		}
		
		40km {
			sama alku kuin 20km mutta huoltopisteen jälkeen ei käännytä selluntielle vaan...:
			sorsasalontie
			takojantie
			karatie
			joensuuntie
			lentokentäntie
			siltasalmentie
			viitonen
			takojantie
			sorsasalontie
			ja tuttua reittiä takaisin
		}
		
		kallaveden kierros (100km) {
			sama alku kuin 40km mutta ei käännytä lentokentäntielle vaan jatketaan joensuuntietä
			vehmersalmentie
			puutossalmentie
			5370
			lossi
			puutossalmentie
			vitostie
			loppua en muista (merkinnän mukaan joko kolmisopen tai leväsentien kautta)
		}
	}

	vielä järjestettävä:
	huoltoauto: jokin pakettiauto johon ehkä ensiapuhenkilöstöä ja jolla kuljetetaan hajonneet pyörät ja pyöräilijöitä
	ensiapu: {
		- neljä päivystäjää olisi 300€ luokkaa
		- kaksi päivystäjää olisi noin 150€ luokkaa
		kyseinen viikonloppu on melko täynnä tapahtumia, joten on selvityksessä riittääkö tekijöitä
		- heidän mukaansa vähintään lähdössä/maalissa tulisi olla ja he suosittelivat että reitin varrellakin olisi piste joko huoltopisteellä tai huoltoautossa (huoltopiste oli loogisempi)
	}

	pyöränhuoltopiste kampuksille: ?

	tapahtumapäivänä tarvittavia tekijöitä: {
		huoltoautoon kuski: 
		pyöränhuoltopisteelle huoltotaitoinen kaveri avustamaan: puolisen tuntia ennakkoon ennen lähtöä
		lähtöön joku antamaan ohjeita:
		joku "julistamaan" lähdön:
		huoltopisteelle ainakin kaksi henkilöä jakamaan vettä?:
		hallituslaisilla mahdollisuutta autella?

		ei välttämättömiä {
			kuvaaja
		}
	}

	hankittavaa: {
		katuliidut
		kylttimateriaalit {
			leveä tussi
			kylttipylväät
			laminointi?
			tulostus
		}
		numerolaput {
			suunnittelu ja jako
		}
		pöytä huoltopisteelle
	}

	numerolaput: {
		numerolappujen palautuksella kontrolloidaan että kaikki pääsevät maaliin
		lappuihin: { 
			huoltoauton/ensiapuauton puhelinnumero
			hätänumero
			qr koodi karttaan
		}
	}

	jimi hoitaa kuopiossa {
		- reitin merkkaajien hankinta: pyöräilyseurat? + avustaja?
		- ainejärjestö joka tulee avustamaan ja hankkii viisi tekijää
		tapahtumapäivälle sekä yhden varatekijän - korvaukseksi ilmainen lukeman käyttökerta
	}

	muutamia sponsoreita voisi kysyä reitin varrelta reittikartalle ja huoltopisteelle

	tapahtumapäivänä kirjattava {
		lähteneiden määrä
		huoltopisteellä käyneet
		maaliin päässeet (annetaan haalarimerkki)
	}

	nimilistat ja numerolaput hoitaa jimi

	kontaktoi hoitaja-opiskelijat savonialta ja kuolo puhelimitse {
		tarvikkeiden saatavuudet, niklakselta lista
	}

	numerolappuihin adan numero huoltoon, muut ongelmat jimi
	kahvia

	uefin pyöränhuoltopisteelle opaste

	hankittava {
		lämmintä juomaa ja jotain evästä maaliin
		uefin suklaata
		kylmäpussi
		banaaneja
		pikkurusinapaketteja
	}

	joensuun reitit {
		lyhyt: {
			
		}
	}

